# Dictator

Dictator begins with the player becoming dictator of a small nation. The previous leader was much loved and you have large boots to fill! The people are happy, wealthy and well fed. Your job is to keep them that way or face assassination.
## Developer Notes

To install this project locally...
* ```git clone https://gitlab.com/megaresp/dictator.git```
* ```cd dictator```
* ```npm install```

## Playing the game
* ```npm run dev```
* [http://localhost:1234](http://localhost:1234)

## Working on the game
* Load the ```dictator/``` project folder in your favourite editor
* Start working on files in ```~/src/```
