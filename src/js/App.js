import { StrictMode } from "react";
import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Gameover from "./component/Gameover";
import Dictator from "./component/Dictator";
import PageNotFound from "./component/PageNotFound";

const App = () => {
  return (
    <StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/gameover" element={<Gameover />} />
          <Route path="/" element={<Dictator />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
    </StrictMode>
  );
};

render(<App />, document.getElementById("root"));
