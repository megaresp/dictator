/**
 * Returns the string chopped at the first space and
 * forced to lowercase
 */
export const chop = (str) => {
  return str.split(" ")[0].toLowerCase();
};
/**
 * Returns the sum of all budgeted expenditure
 */
export const calcExpenditure = (budget) => {
  return (
    budget.calcNextDebtRepayment[1] +
    budget.calcPension[1] +
    budget.wage[1] * (budget.calcPopDist[1] * budget.pop[1] * budget.wage[2]) +
    budget.defence * budget.calcTaxRevenue[1] +
    budget.family * budget.calcTaxRevenue[1] +
    budget.farming * budget.calcTaxRevenue[1] +
    budget.immigration * budget.calcTaxRevenue[1] +
    budget.infrastructure * budget.calcTaxRevenue[1] +
    budget.law * budget.calcTaxRevenue[1] +
    budget.propaganda * budget.calcTaxRevenue[1]
  );
};

/**
 * Returns decimal with digits numbers after the deciminal point
 */
export const commas = (decimal, digits = 0) => {
  const formatter = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: digits,
    maximumFractionDigits: digits,
  });

  return formatter.format(decimal);
};

/**
 * Returns decimal expressed as $#,###.##
 * If showCents is false returns $#,###
 */
export const currency = (decimal, showCents = false) => {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: showCents ? 2 : 0,
    maximumFractionDigits: showCents ? 2 : 0,
  });

  return formatter.format(decimal);
};

/**
 * Returns an empty form object
 */
export const emptyForm = (form) => {
  const keys = Object.keys(form);
  const keysCount = keys.length;

  for (let i = 0; i < keysCount; i++) {
    if (keys[i] === "repayDebt") {
      continue;
    } else {
      form[keys[i]]["value"];
    }
  }

  return form;
};

/**
 * Injects message into the appropriate budget form field and displays
 */
export const error = (message, domId) => {
  const elField = document.getElementById(domId);
  elField.innerText = `${message}.`;
  elField.style.display = "block";
};

/**
 * Returns false if at least one form item contains a value. True otherwise
 */
export const formIsEmpty = (form) => {
  const keys = Object.keys(form);
  const keysCount = keys.length;

  for (let i = 0; i < keysCount; i++) {
    if (keys[i] === "repayDebt") {
      continue;
    } else if (form[keys[i]]["value"] && form[keys[i]]["value"] !== 0) {
      return false;
    }
  }

  return true;
};

/**
 * Sets display none on all elements in selector
 */
export const hide = (selector) => {
  const els = document.querySelectorAll(selector);

  if (!els) {
    return;
  }

  for (const i in els) {
    if (typeof els[i] === "object" && els[i].style) {
      els[i].style.display = "none";
    }
  }
};

/**
 * Returns a saved game from localStorage (if it exists) in the form of a JSON object
 */
export const load = (key) => {
  if (!localStorage[`dictator${key}`]) {
    return null;
  }

  let retObj = JSON.parse(localStorage[`dictator${key}`]);
  retObj.budget = JSON.parse(retObj.budget);
  return retObj;
};

export const newGame = () => {
  if (localStorage["dictatorSaveGame"]) {
    localStorage.removeItem("dictatorSaveGame");
  }
  location.href = "/";
};

/**
 * Displays a message at the top of the page below the hero
 */
export const notify = (title, message) => {
  document.getElementById(
    "update-notice"
  ).innerHTML = `<h3>${title}</h3><p>${message}</p>`;
  document.getElementById("update-notice").style.display = "block";
};

/**
 * Returns a number in the form #,###.#%
 * The number of digits can be customised in the optional digits parameter
 */
export const percent = (decimal, digits = 2) => {
  return `${commas(decimal * 100, digits)}%`;
};

/**
 * Returns 's' for any number except 1
 */
export const plural = (num) => {
  return num === 1 ? "" : "s";
};

/**
 * Saves the game to localStorage
 */
export const save = (key, budget, year, quarter, reason = null) => {
  const savedObject = {
    budget: JSON.stringify(budget),
    year: year,
    quarter: quarter,
    reason: reason,
  };
  localStorage[`dictator${key}`] = JSON.stringify(savedObject);
};

/**
 * Moves every array item left one spot
 */
export const shiftArrayLeft = (arr, newValue) => {
  for (let i = 1; i < arr.length; i++) {
    arr[i - 1] = arr[i];
  }
  arr[arr.length - 1] = newValue;
  return arr;
};

/**
 * Reveals a tool tip
 */
export const showTip = (mode, domId) => {
  const el = document.getElementById(`info-${domId}`);

  if (mode) {
    el.classList.add("fadeIn");
  } else {
    el.classList.add("fadeOut");
    setTimeout(() => {
      el.classList.remove("fadeIn");
      el.classList.remove("fadeOut");
    }, 550);
  }
};

/**
 * Converts a string into a slug
 */
export const slugify = (str) => {
  return str
    .toString() // Cast to string
    .toLowerCase() // Convert the string to lowercase letters
    .normalize("NFD") // The normalize() method returns the Unicode Normalization Form of a given string.
    .trim() // Remove whitespace from both sides of a string
    .replace(/\s+/g, "") // Replace spaces with ""
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-"); // Replace multiple - with single -
};
