import { currency } from "../lib/Lib";

const NetRevenue = (props) => {
  const { netRevenue } = props;

  return (
    <span className={`${netRevenue < 0 ? "text-alert" : "text-prime"}`}>
      {currency(netRevenue)}
    </span>
  );
};

export default NetRevenue;
