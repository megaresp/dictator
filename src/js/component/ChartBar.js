import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";

const ChartBar = (props) => {
  const { title, data, labels, bounce } = props;

  const barColours = [
    "rgb(158, 0, 89)",
    "rgb(237, 174, 73)",
    "rgb(56, 102, 65)",
    "rgb(48, 99, 142)",
    "rgb(0, 61, 91)",
    "rgb(243, 30, 2)",
  ];

  ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
  );

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
        position: "",
      },
      title: {
        display: false,
        text: "",
      },
    },
  };

  const chartData = {
    labels,
    datasets: [
      {
        label: "",
        data: data,
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: barColours,
      },
    ],
  };

  return (
    <div className={`chart grow grow2`} data-bounce="1">
      <div className="chart-title">{title}</div>
      <div className="chart-content">
        <Bar options={options} data={chartData} />
      </div>
    </div>
  );
};

export default ChartBar;
