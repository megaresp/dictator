import { Link } from "react-router-dom";
import Hero from "./Hero";
import { newGame } from "../lib/Lib";

const PageNotFound = () => {
  return (
    <>
      <Hero
        title="Page not found"
        year={null}
        quarter={null}
        message="Sorry, the requested page doesn’t exist."
        newGame={newGame}
        saveGame={null}
      />
      <main className="pt-5">
        <h1 className="text-2xl font-semibold text-altFg">
          Sorry, the requested page doesn’t exist.
        </h1>
        <p className="mt-4 text-lg font-medium text-altFg">
          <Link to="/" className="text-alert hover:text-prime">
            Click here
          </Link>{" "}
          and play Dictator.
        </p>
      </main>
    </>
  );
};

export default PageNotFound;
