/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { useState } from "react";
import { slugify } from "../lib/Lib";

const RatioCard = (props) => {
  const { back, cost, image, tax, title } = props;
  const [front, setFront] = useState(true);

  return (
    <div
      className={`ratio-card box-shadow ${slugify(title)}${
        front ? "" : " rotate"
      }`}
      onClick={() => {
        setFront(!front);
      }}
      data-rotate="1"
    >
      <div className="front">
        <div className="title flex items-center">{title}</div>
        <div className="image" style={{ backgroundImage: `url(${image}) ` }}>
          &nbsp;
        </div>
        <div className="data flex">
          <div className="tax flex items-center">{tax}</div>
          <div className="cost flex items-center">{cost}</div>
        </div>
      </div>
      <div className="back">
        <div className="title flex items-center">{title}</div>
        <div className="info" dangerouslySetInnerHTML={{ __html: back }}></div>
        <div className="data flex">
          <div className="tax flex items-center">{tax}</div>
          <div className="cost flex items-center">{cost}</div>
        </div>
      </div>
    </div>
  );
};

export default RatioCard;
