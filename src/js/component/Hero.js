import { save } from "../lib/Lib.js";
import Button from "./Button";

const Hero = (props) => {
  const { title, year, quarter, message, newGame, budget } = props;

  const saveGame = () => {
    const shade = document.getElementById("shade");
    const shadeMsg = document.getElementById("shadeMsg");
    shadeMsg.innerHTML =
      '<p>Saving game.<br />Please wait&hellip;</p><img id="loading" class="rotate" src="img/loading.svg" alt="Loading..." />';
    shadeMsg.style.visibility = "visible";
    shade.classList.add("fadeIn");

    /*
     * Use requestAnimationFrame() to drive transform: rotateZ(Ndeg);
     */

    save("SaveGame", budget, year, quarter);

    setTimeout(() => {
      shade.classList.add("fadeOut");
      setTimeout(() => {
        shadeMsg.style.visibility = "hidden";
      }, 550);
      setTimeout(() => {
        shadeMsg.style.visibility = "hidden";
        shade.classList.remove("fadeOut");
        shade.classList.remove("fadeIn");
      }, 600);
    }, 1200);
  };

  return (
    <div className="hero box-shadow">
      <div className="constrain">
        <h1>{title}</h1>
        <div className="time-counter">
          {year && quarter ? `Year of rule: ${year}.${quarter}` : null}
        </div>
        {message ? <h3 className="game-over">{message}</h3> : null}
        <div className="controls">
          <Button callback={newGame} label="New Game" />
          {message ? null : (
            <Button
              callback={saveGame}
              label="Save Game"
              attributes={{ type: "button" }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default Hero;
