import Subheading from "./Subheading";
// import BarGraph from "./BarGraph";
import ChartBar from "./ChartBar";
import ChartLine from "./ChartLine";

const DistributionGraphs = (props) => {
  const { budget } = props;

  const wageBill =
    budget.wage[1] * budget.calcPopDist[1] * budget.pop[1] * budget.wage[2];

  const popDistLabels = ["Children", "Working", "Unemployed", "Retired"];
  const taxDistLabels = ["Govt wages", "Penion", "Debt", "Infrastructure"];
  const rawLabels = [];
  // for (let i = budget.calcCrime.length - 1; i >= 0; i--) {
  for (let i = 1; i < budget.calcCrime.length; i++) {
    rawLabels[i] = `${i + 1} quarters ago`;
  }
  rawLabels[0] = "Last quarter";
  const quarterlyLabels = rawLabels.reverse();

  const taxDistData = [
    wageBill / budget.calcTaxRevenue[1],
    budget.calcPension[1] / budget.calcTaxRevenue[1],
    budget.debt / budget.calcTaxRevenue[1],
    (budget.infrastructure * budget.calcTaxRevenue[1]) /
      budget.calcTaxRevenue[1],
  ];
  const crimeData = [];
  for (let i = 0; i < budget.calcCrime.length; i++) {
    crimeData[i] = { x: 0, y: budget.calcCrime[i] };
  }
  const inflationData = [];
  for (let i = 0; i < budget.calcInflation.length; i++) {
    inflationData[i] = { x: 0, y: budget.calcInflation[i] };
  }

  return (
    <section className="distribution-graphs">
      <Subheading label="Charts:" />
      <div className="charts">
        <ChartBar
          title="Population distribution"
          data={budget.calcPopDist}
          labels={popDistLabels}
          bounce="bounce2"
        />
        <ChartBar
          title="Percent of tax revenue"
          data={taxDistData}
          labels={taxDistLabels}
          bounce="bounce4"
        />
        <ChartLine
          title="Quarterly crimes/1,000"
          data={crimeData}
          labels={quarterlyLabels}
          bounce="bounce6"
        />
        <ChartLine
          title="Quarterly inflation"
          data={inflationData}
          labels={quarterlyLabels}
          bounce="bounce8"
        />
      </div>
    </section>
  );
};

export default DistributionGraphs;
