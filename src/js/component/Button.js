import { v4 as uuidv4 } from "uuid";

const Button = (props) => {
  const {
    callback,
    label,
    attributes = { id: uuidv4(), type: "button", classes: "" },
  } = props;
  const { id, type } = attributes;

  return (
    <button id={id} type={type} onClick={callback}>
      {label}
    </button>
  );
};

export default Button;
