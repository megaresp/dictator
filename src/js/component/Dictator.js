import { useState, useEffect } from "react";
import { load, newGame } from "../lib/Lib";
import Hero from "./Hero";
import BudgetObject from "../modules/BudgetObject";
import Report from "./Report";
import Budget from "./Budget";

const Dictator = () => {
  const [year, setYear] = useState();
  const [quarter, setQuarter] = useState();
  const [form, setForm] = useState();
  const formData = {};

  useEffect(() => {
    setYear(1);
    setQuarter(1);

    // Remove the last GameOver saved game
    if (localStorage["dictatorGameOver"]) {
      localStorage.removeItem("dictatorGameOver");
    }

    // Retrieve a saved game from local storage
    if (localStorage["dictatorSaveGame"]) {
      const savedGame = load("SaveGame");
      setYear(savedGame.year);
      setQuarter(savedGame.quarter);
      for (const [key, value] of Object.entries(savedGame.budget)) {
        BudgetObject[key] = value;
      }
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  /**
   *
   * Below is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   * Below is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   * Below is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   *
   */
  useEffect(() => {
    if (form) console.log("~~~~+~~~~+~~~~+~~~~+~~~~");
    console.dir(BudgetObject);
    if (form) console.dir(form);
  }, [form]);
  /**
   *
   * Above is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   * Above is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   * Above is only for debugging. REMOVE LATER! REMOVE LATER! REMOVE LATER!
   *
   */

  const handleBudgetSubmit = (event) => {
    event.preventDefault();
    const tgt = event.target;

    Object.keys(tgt).map((key) => {
      if (
        !isNaN(parseInt(key)) &&
        tgt[key].id !== "" &&
        tgt[key].id.substr(0, 6) !== "submit"
      ) {
        formData[tgt[key].id] = {
          domId: tgt[key].id,
          value: Number(tgt[key].value),
        };
      }
    });

    setForm(formData);
  };

  return (
    <>
      <Hero
        title="Dictator!"
        year={year}
        quarter={quarter}
        message={null}
        newGame={newGame}
        budget={BudgetObject}
      />
      <main>
        <div id="update-notice"></div>
        <Report
          budget={BudgetObject}
          form={form}
          setForm={setForm}
          year={year}
          setYear={setYear}
          quarter={quarter}
          setQuarter={setQuarter}
        />
        <Budget
          budget={BudgetObject}
          form={form}
          setForm={setForm}
          handleBudgetSubmit={handleBudgetSubmit}
        />
      </main>
    </>
  );
};

export default Dictator;
