import Subheading from "./Subheading";
import NetRevenue from "./NetRevenue";
import { currency, calcExpenditure } from "../lib/Lib";

const RevenueAndExpenditure = (props) => {
  const { budget, newBorrowing } = props;

  const grossRevenue =
    budget.calcTaxRevenue[1] + newBorrowing + budget.treasury[1];
  const totalExpenditure = calcExpenditure(budget);
  const netRevenue = grossRevenue - totalExpenditure;
  const wageBill =
    budget.wage[1] * budget.calcPopDist[1] * budget.pop[1] * budget.wage[2];
  const quarterlyReport = `<span class="${
    netRevenue < 0 ? "text-alert" : "text-prime"
  }">Quarterly ${netRevenue < 0 ? "shortfall" : "surplus"} ${currency(
    netRevenue
  )}</span>`;

  return (
    <section className="renue-expenditure">
      <Subheading label="Revenue &amp; Expenditure:" />
      <p
        className="quarterly-report"
        dangerouslySetInnerHTML={{ __html: quarterlyReport }}
      ></p>

      {/* Fake table to allow responsiveness */}
      <div className="table grow box-shadow">
        <div className="col revenue">
          <div className="col-title">Revenue</div>
          <div className="col-wrap">
            <div className="col-12">
              <div className="row body head">Item</div>
              <div className="row body item-label">Taxation</div>
              <div className="row body item-label">New Borrowings</div>
              <div className="row body item-label">Treasury</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body foot">{currency(grossRevenue)}</div>
            </div>
            <div className="col-12">
              <div className="row body head">Amount</div>
              <div className="row body">
                {currency(budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">{currency(newBorrowing)}</div>
              <div className="row body">{currency(budget.treasury[1])}</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body">&nbsp;</div>
              <div className="row body foot">{currency(grossRevenue)}</div>
            </div>
          </div>
        </div>
        <div className="col expenditure">
          <div className="col-title">Expenditure</div>
          <div className="col-wrap">
            <div className="col-12">
              <div className="row body head">Item</div>
              <div className="row body item-label">Next debt payment</div>
              <div className="row body item-label">Defence</div>
              <div className="row body item-label">Family</div>
              <div className="row body item-label">Farming</div>
              <div className="row body item-label">Immigration</div>
              <div className="row body item-label">Infrastructure</div>
              <div className="row body item-label">Law and enforcement</div>
              <div className="row body item-label">Propaganda</div>
              <div className="row body item-label">Pension</div>
              <div className="row body item-label">Government wages</div>
              <div className="row body foot">Total expenses</div>
            </div>
            <div className="col-12">
              <div className="row body head">Amount</div>
              <div className="row body">
                {currency(budget.calcNextDebtRepayment[1])}
              </div>
              <div className="row body">
                {currency(budget.defence * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.family * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.farming * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.immigration * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.infrastructure * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.law * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">
                {currency(budget.propaganda * budget.calcTaxRevenue[1])}
              </div>
              <div className="row body">{currency(budget.calcPension[1])}</div>
              <div className="row body">{currency(wageBill)}</div>
              <div className="row body foot">{currency(totalExpenditure)}</div>
            </div>
          </div>
        </div>
      </div>
      <div className="net-revenue">
        Net Revenue: <NetRevenue netRevenue={netRevenue} />
      </div>
    </section>
  );
};

export default RevenueAndExpenditure;
