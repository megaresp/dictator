const Delta = (props) => {
  const { change } = props;

  return (
    <span
      className={`delta ml-1 ${
        change < 0 ? "text-alert" : change > 0 ? "text-prime" : "charcoal"
      }`}
    >
      {change < 0 ? "▼" : change > 0 ? "▲" : "◇"}
    </span>
  );
};

export default Delta;
