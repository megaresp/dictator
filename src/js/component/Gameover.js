import { useEffect } from "react";
import { load, newGame } from "../lib/Lib";
// import Button from "./Button";
import Hero from "./Hero";
import RevenueAndExpenditure from "./RevenueAndExpenditure";
import NationalData from "./NationalData";
import GovernmentDebt from "./GovernmentDebt";
import RatioCards from "./RatioCards";
import DistributionGraphs from "./DistributionGraphs";

const Gameover = () => {
  const gameOverObj = load("GameOver");
  const budget = gameOverObj.budget;
  const year = gameOverObj.year;
  const quarter = gameOverObj.quarter;
  const reason = gameOverObj.reason;

  useEffect(() => {
    console.log("Game over budget from local storage:");
    console.dir(budget);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  let heroMessage =
    "Your rule has ended rather more abruptly than either of us expected.";
  if (year === 1 && quarter === 1) {
    heroMessage =
      "It takes a ‘special’ kind of intellect to lose on the very first turn!";
  } else if (year >= 10) {
    heroMessage =
      "Congratulations! You lasted longer than most wannabe dictators.";
  }

  return (
    <>
      <Hero
        title="Game Over!"
        year={year}
        quarter={quarter}
        message={heroMessage}
        newGame={newGame}
        saveGame={null}
      />
      <main>
        <div className="report">
          <div className="explanation pt-6">
            <p className="gameover-reason">{reason}</p>
            <p className="mt-3">
              Your ‘loyal’ citizens, fed up with such ineptitude, decided to
              take matters into their own hands and have (cough) ‘terminated’
              your premiership in a very literal sense.
            </p>
            <p className="mt-3">
              Your former subjects now enjoy the benefits and privileges of life
              in a constitutional democracy while your remains nourish the local
              flora.
            </p>
          </div>
          <h2 className="mt-10 text-4xl font-medium">
            <a href="/" className="text-prime hover:text-lightFg">
              Click here and play again.
            </a>
          </h2>

          <NationalData budget={budget} />
          <RatioCards budget={budget} />
          <DistributionGraphs budget={budget} />
          <GovernmentDebt budget={budget} />
          <RevenueAndExpenditure budget={budget} newBorrowing={0} />
          <br />
        </div>
      </main>
    </>
  );
};

export default Gameover;
