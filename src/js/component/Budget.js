import Subheading from "./Subheading";
import BudgetForm from "./BudgetForm";

const Budget = (props) => {
  const { budget } = props;
  const { form } = props;
  // const { setForm } = props;
  const { handleBudgetSubmit } = props;

  return (
    <section className="budget">
      <Subheading label="Quarterly Budget:" />
      <BudgetForm
        budget={budget}
        form={form}
        // setForm={setForm}
        handleBudgetSubmit={handleBudgetSubmit}
      />
    </section>
  );
};

export default Budget;
