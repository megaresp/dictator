import Subheading from "./Subheading";
import { currency, percent } from "../lib/Lib";

const GovernmentDebt = (props) => {
  const { budget } = props;

  return (
    <section className="government-debt">
      <Subheading label="Government Debt:" />
      <table className="box-shadow grow">
        <thead>
          <tr>
            <th>Item</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Total owing</td>
            <td>{currency(budget.debt)}</td>
          </tr>
          <tr>
            <td>Annual interest</td>
            <td>{percent(budget.debtInterest * 4, 3)}</td>
          </tr>
          <tr>
            <td>Minimum annual principle</td>
            <td>{percent(budget.debtPrinciple * 4, 3)}</td>
          </tr>
          <tr>
            <td>Next quarterly repayment</td>
            <td>{currency(budget.calcNextDebtRepayment[1])}</td>
          </tr>
          <tr>
            <td>Debt remaining after repayment</td>
            <td>{currency(budget.debt - budget.calcNextDebtRepayment[1])}</td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};

export default GovernmentDebt;
