import { useState, useEffect } from "react";
import { currency } from "../lib/Lib";
import BudgetField from "./BudgetField";
import Button from "./Button";

const BudgetForm = (props) => {
  const { budget, form, handleBudgetSubmit } = props;

  const [borrow, setBorrow] = useState(0);
  const [repay, setRepay] = useState(0);
  const [tax, setTax] = useState(0);
  const [wages, setWages] = useState(0);
  const [pension, setPension] = useState(0);
  const [defence, setDefence] = useState(0);
  const [family, setFamily] = useState(0);
  const [farming, setFarming] = useState(0);
  const [immigration, setImmigration] = useState(0);
  const [infrastructure, setInfrastructure] = useState(0);
  const [law, setLaw] = useState(0);
  const [propaganda, setPropaganda] = useState(0);

  useEffect(() => {
    setBorrow(0);
    setRepay(budget.calcNextDebtRepayment[1]);
    setTax(budget.tax[1]);
    setWages(budget.wage[1]);
    setPension(budget.pension[1]);
    setDefence(budget.defence);
    setFamily(budget.family);
    setFarming(budget.farming);
    setImmigration(budget.immigration);
    setInfrastructure(budget.infrastructure);
    setLaw(budget.law);
    setPropaganda(budget.propaganda);
  }, [form]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <form id="budgetForm" className="budgetForm" onSubmit={handleBudgetSubmit}>
      <div id="formError"></div>
      <BudgetField
        title="New borrowing"
        id="newBorrowing"
        type="number"
        range={[0, null, null, borrow]}
        onChange={setBorrow}
        placeholder="Enter amount to borrow"
        info="Short on funds? Enter the amount you’d like to borrow in this field."
        onClick={null}
      />
      <BudgetField
        title="Pay down debt"
        id="repayDebt"
        type="number"
        range={[
          parseFloat(repay).toFixed(0),
          null,
          null,
          parseFloat(repay).toFixed(0),
        ]}
        onChange={setRepay}
        placeholder="Enter amount to repay"
        info={`How much debt would you like to repay? The minimum repayment is ${currency(
          budget.calcNextDebtRepayment[1]
        )}`}
      />
      <BudgetField
        title="Tax rate"
        id="taxRate"
        type="number"
        range={[0, 1, 0.001, tax]}
        onChange={setTax}
        placeholder="Enter new tax rate"
        info="The percentage of their wages each citizen must pay in tax. This is a number between 0 and 1."
        onClick={null}
      />
      <BudgetField
        title="Wages"
        id="wages"
        type="number"
        range={[0, null, null, wages]}
        onChange={setWages}
        placeholder="Enter new average wage"
        info="As dictator you can raise or lower the national average wage at will. Simply enter the new wage here."
        onClick={null}
      />
      <BudgetField
        title="Pension ratio"
        id="pensionRatio"
        type="number"
        range={[0, 1, 0.0001, pension]}
        onChange={setPension}
        placeholder="Set the pension as a % of the average wage"
        info="Enter a decimal number between 0 and 1 to set the pension as a percentage of the average wage."
        onClick={null}
      />
      <BudgetField
        title="Defence"
        id="defenceRatio"
        type="number"
        range={[0, 1, 0.0001, defence]}
        onChange={setDefence}
        placeholder="Set the % of tax for defence"
        info="Set the percentage of tax to defend the realm from foreign invaders."
        onClick={null}
      />
      <BudgetField
        title="Family"
        id="familyRatio"
        type="number"
        range={[0, 1, 0.0001, family]}
        onChange={setFamily}
        placeholder="Set the % of tax to encourage families"
        info="Set the percentage of tax to promote families and encourage the nation’s birthrate."
        onClick={null}
      />
      <BudgetField
        title="Farming"
        id="farmingRatio"
        type="number"
        range={[0, 1, 0.0001, farming]}
        onChange={setFarming}
        placeholder="Set the % of tax to encourage food production"
        info="Set the percentage of tax to promote farming and encourage food production."
        onClick={null}
      />
      <BudgetField
        title="Immigration"
        id="immigrationRatio"
        type="number"
        range={[0, 1, 0.0001, immigration]}
        onChange={setImmigration}
        placeholder="Set the % of tax to promote immigration"
        info="Set the percentage of tax to promote immigration and encourage population growth."
        onClick={null}
      />
      <BudgetField
        title="Infrastructure"
        id="infrastructureRatio"
        type="number"
        range={[0, 1, 0.0001, infrastructure]}
        onChange={setInfrastructure}
        placeholder="Set the % of tax to maintain and improve infrastructure"
        info="Set the percentage of tax to maintain infrastructure, improve it, and provode other government services."
        onClick={null}
      />
      <BudgetField
        title="Law and Law Enforcement"
        id="lawRatio"
        type="number"
        range={[0, 1, 0.0001, law]}
        onChange={setLaw}
        placeholder="Set the % of tax for law and law enforcement"
        info="Set the percentage of tax to enforce the law and maintain public order."
        onClick={null}
      />
      <BudgetField
        title="Propaganda"
        id="propagandaRatio"
        type="number"
        range={[0, 1, 0.0001, propaganda]}
        onChange={setPropaganda}
        placeholder="Set the % of tax to promote your regime"
        info="Set the percentage of tax to promote the benefits of your regime and downplay problems."
        onClick={null}
      />
      <label htmlFor="submitBudget">
        <p className="title">&nbsp;</p>
        <Button
          callback={null}
          label="Submit Budget"
          attributes={{ id: "submitBudget", type: "submit", classes: "" }}
        />
      </label>
    </form>
  );
};

export default BudgetForm;
