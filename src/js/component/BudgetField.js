import { showTip } from "../lib/Lib";

const BudgetField = (props) => {
  const { title, id, type, range, placeholder, info, onChange } = props;

  return (
    <>
      <div className="formFieldError" id={`error-${id}`}></div>
      <label htmlFor={id}>
        <p className="title">{title}:</p>
        {onChange ? (
          <input
            id={id}
            type={type}
            min={range[0] !== null ? range[0] : null}
            max={range[1] !== null ? range[1] : null}
            step={range[2] !== null ? range[2] : null}
            onChange={(e) => onChange(e.target.value)}
            value={range[3]}
            placeholder={`${placeholder}…`}
          />
        ) : (
          <input
            id={id}
            type={type}
            min={range[0] !== null ? range[0] : null}
            max={range[1] !== null ? range[1] : null}
            step={range[2] !== null ? range[2] : null}
            placeholder={`${placeholder}…`}
          />
        )}
        <button
          className="infoIcon"
          type="button"
          onMouseEnter={() => showTip(true, id)}
          onMouseLeave={() => showTip(false, id)}
        >
          ?
        </button>
        <p className="info" id={`info-${id}`}>
          {info}
        </p>
      </label>
    </>
  );
};

export default BudgetField;
