import { useState, useEffect } from "react";
import Subheading from "./Subheading";
import RatioCard from "./RatioCard";
import { currency, percent } from "../lib/Lib";

const RatioCards = (props) => {
  const { budget } = props;
  const [targetId, setTargetId] = useState(null);

  const setTarget = (e) => {
    setTargetId(e.target.parentElement.id);
  };

  useEffect(() => {
    if (!targetId) return;

    const ele = document.getElementById(targetId);
    if (ele === null) return;

    const isFlipped = ele.classList.contains("rotate");

    if (isFlipped) {
      ele.classList.remove("rotate");
    } else {
      ele.classList.add("rotate");
    }

    setTargetId(null);
  }, [targetId]);

  return (
    <section className="ratios">
      <Subheading label="Ratios (click card for more info)" />
      <div className="cards flex flex-wrap">
        <RatioCard
          title="Defence"
          image="../img/defence.jpg"
          back="<p>Defence of the Realm is the first duty of government. A wise ruler invests enough to keep foreign invaders at bay without blowing the budget."
          tax={percent(budget.defence, 3)}
          cost={currency(budget.defence * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Family"
          image="../img/family.jpg"
          back="<p>Family stability contributes to happiness, employment, low crime and a healthy birthrate.</p><p>The data below show both the percentage and the dollars currently invested in promoting the family."
          tax={percent(budget.family, 3)}
          cost={currency(budget.family * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Farming"
          image="../img/farming.jpg"
          back="<p>Farming is essential for the nation’s food security. Your subjects won’t get enough to eat if agriculture isn’t viable.</p>"
          tax={percent(budget.farming, 3)}
          cost={currency(budget.farming * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Immigration"
          image="../img/immigration.jpg"
          back="<p>Immigration is a vital component of the nation’s economic health, yet it’s also a contentious issue for many citizens. Too much immigration leads to crime, lower wages and decreased happiness; too little risks economic stagnation.</p>"
          tax={percent(budget.immigration, 3)}
          cost={currency(budget.immigration * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Infrastructure"
          image="../img/infrastructure.jpg"
          back="<p>Infrastructure includes roads, buildings and government services. Investment is needed to keep the economy moving, but too much may lead to a bloated public sector, corruption and stagnation.</p>"
          tax={percent(budget.infrastructure, 3)}
          cost={currency(budget.infrastructure * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Law Enforcement"
          image="../img/law.jpg"
          back="<p>As a dictator, your word is the law. But talk is cheap, and you’ll need police officers, a legal system and prisons to enforce the law.</p>"
          tax={percent(budget.law, 3)}
          cost={currency(budget.law * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Pension"
          image="../img/pension.jpg"
          back="<p>Your citizens expect a healthy pension in retirement. But don’t be too generous. The pension bill will quickly drain the treasury as the population ages.</p>"
          tax={percent(budget.pension[1], 3)}
          cost={currency(budget.calcPension[1])}
          setTarget={setTarget}
        />
        <RatioCard
          title="Propaganda"
          image="../img/propaganda.jpg"
          back="<p>Every dictator needs to keep on the right side of the people. A propaganda program ensures they know why you’re the right person for the job. Take care not to overspend in case your efforts backfire.</p>"
          tax={percent(budget.propaganda, 3)}
          cost={currency(budget.propaganda * budget.calcTaxRevenue[1])}
          setTarget={setTarget}
        />
      </div>
    </section>
  );
};

export default RatioCards;
