import { useEffect } from "react";
import UpdateBudget from "../modules/UpdateBudget";
import NationalData from "./NationalData";
import GovernmentDebt from "./GovernmentDebt";
import RatioCards from "./RatioCards";
import RevenueAndExpenditure from "./RevenueAndExpenditure";
import DistributionGraphs from "./DistributionGraphs";

const Report = (props) => {
  const { budget, form, setForm, year, setYear, quarter, setQuarter } = props;

  let newBorrowing = 0;
  if (form && form.newBorrowing.value > 0) {
    newBorrowing = form.newBorrowing.value;
  }

  // If there is form data go ahead and process it
  useEffect(() => {
    if (form) {
      UpdateBudget(budget, form, setForm, year, setYear, quarter, setQuarter);
    }
  }, [budget, form]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="report">
      <NationalData budget={budget} />
      <RatioCards budget={budget} />
      <DistributionGraphs budget={budget} />
      <GovernmentDebt budget={budget} />
      <RevenueAndExpenditure budget={budget} newBorrowing={newBorrowing} />
    </div>
  );
};

export default Report;
