import Subheading from "./Subheading";
import Delta from "./Delta";
import { commas, currency, percent } from "../lib/Lib";

const NationalData = (props) => {
  const { budget } = props;

  const inflationChange =
    (budget.calcInflation[9] - budget.calcInflation[8]) /
    budget.calcInflation[8];
  const colChange =
    (budget.calcCostLiving[1] - budget.calcCostLiving[0]) /
    budget.calcCostLiving[0];
  const popChange = (budget.pop[1] - budget.pop[0]) / budget.pop[0];
  const ageChange = (budget.age[1] - budget.age[0]) / budget.age[0];
  const wageChange = (budget.wage[1] - budget.wage[0]) / budget.wage[0];
  const pensionChange =
    (budget.pension[1] - budget.pension[0]) / budget.pension[0];
  const taxChange = (budget.tax[1] - budget.tax[0]) / budget.tax[0];
  const foodChange = (budget.food[1] - budget.food[0]) / budget.food[0];
  const healthChange =
    (budget.calcHealth[1] - budget.calcHealth[0]) / budget.calcHealth[0];
  const happinessChange =
    (budget.calcHappiness[1] - budget.calcHappiness[0]) /
    budget.calcHappiness[0];

  return (
    <section className="national-data">
      <Subheading label="National Data:" />
      <table className="box-shadow grow">
        <thead>
          <tr>
            <th className="">Item</th>
            <th className="">Amount</th>
            <th>Change</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Average wage</td>
            <td>{currency(budget.wage[1])}</td>
            <td>
              {percent(wageChange)}
              <Delta change={wageChange} />
            </td>
          </tr>
          <tr>
            <td>State pension</td>
            <td>{currency(budget.pension[1] * budget.wage[1])}</td>
            <td>
              {percent(pensionChange)}
              <Delta change={pensionChange} />
            </td>
          </tr>
          <tr>
            <td>Tax rate</td>
            <td>{percent(budget.tax[1])}</td>
            <td>
              {percent(taxChange)}
              <Delta change={taxChange} />
            </td>
          </tr>
          <tr>
            <td>Inflation</td>
            <td>{percent(budget.calcInflation[9])}</td>
            <td>
              {percent(inflationChange)}
              <Delta change={inflationChange} />
            </td>
          </tr>
          <tr>
            <td>Cost of Living</td>
            <td>{percent(budget.calcCostLiving[1])}</td>
            <td>
              {percent(colChange)}
              <Delta change={colChange} />
            </td>
          </tr>
          <tr>
            <td>Food production (100% is enough to feed everyone)</td>
            <td>{percent(budget.food[1])}</td>
            <td>
              {percent(foodChange)}
              <Delta change={foodChange} />
            </td>
          </tr>
          <tr>
            <td>Health</td>
            <td>{percent(budget.calcHealth[1])}</td>
            <td>
              {percent(healthChange)}
              <Delta change={healthChange} />
            </td>
          </tr>
          <tr>
            <td>Average age</td>
            <td>{commas(budget.age[1], 3)}</td>
            <td>
              {percent(ageChange)}
              <Delta change={ageChange} />
            </td>
          </tr>
          <tr>
            <td>Population</td>
            <td>{commas(budget.pop[1])}</td>
            <td>
              {percent(popChange)}
              <Delta change={popChange} />
            </td>
          </tr>
          <tr>
            <td>Crime rate</td>
            <td>{percent(budget.calcCrime[9] / 1000, 1)}</td>
            <td>
              {percent(
                budget.calcCrime[9] / 1000 -
                  budget.calcCrime[8] / 1000 / (budget.calcCrime[8] / 1000)
              )}
              <Delta change={happinessChange} />
            </td>
          </tr>
          <tr>
            <td>National happiness</td>
            <td>{percent(budget.calcHappiness[1], 1)}</td>
            <td>
              {percent(happinessChange)}
              <Delta change={happinessChange} />
            </td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};

export default NationalData;
