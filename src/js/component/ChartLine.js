import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

const ChartLine = (props) => {
  const { title, data, labels, bounce } = props;

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
        position: "top",
      },
      title: {
        display: false,
        text: "Crime Statistics",
      },
    },
  };

  const chartData = {
    labels,
    datasets: [
      {
        label: "",
        data: data,
        borderColor: "rgb(32, 56, 104)", // "rgb(255, 99, 132)",
        backgroundColor: "rgb(20, 32, 56)", // "rgba(255, 99, 132, 0.5)",
      },
    ],
  };

  return (
    <div className={`chart grow grow2`} data-bounce="1">
      <div className="chart-title">{title}</div>
      <div className="chart-content">
        <Line options={options} data={chartData} />
      </div>
    </div>
  );
};

export default ChartLine;
