const Subheading = (props) => {
  const { label } = props;

  return (
    <h2 className="subheading mt-10 mb-1 text-2xl font-semibold text-prime leading-none">
      <div data-slide-in="1">{label}</div>
    </h2>
  );
};

export default Subheading;
