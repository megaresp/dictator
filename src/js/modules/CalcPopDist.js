const CalcPopDist = (data) => {
  const { age, family, immigration, wage } = data;

  function adjustForAge(popDist) {
    if (age[1] > 55) {
      popDist[0] -= 0.07;
      popDist[1] -= 0.02;
      popDist[2] -= 0.03;
      popDist[3] += 0.13;
    } else if (age[1] > 45) {
      popDist[0] -= 0.04;
      popDist[1] -= 0.01;
      popDist[2] -= 0.02;
      popDist[3] += 0.07;
    } else if (age[1] > 35) {
      popDist[0] -= 0.01;
      popDist[2] -= 0.01;
      popDist[3] += 0.02;
    }
    return popDist;
  }

  function adjustForFamily(popDist) {
    if (family > 0.1) {
      popDist[0] += 0.02;
      popDist[3] -= 0.02;
    } else if (family > 0.075) {
      popDist[0] += 0.015;
      popDist[3] -= 0.015;
    } else if (family > 0.05) {
      popDist[0] += 0.01;
      popDist[3] -= 0.01;
    } else if (family > 0.025) {
      popDist[0] += 0.005;
      popDist[3] -= 0.005;
    } else if (family >= 0.01) {
      popDist[0] += 0.0005;
      popDist[3] -= 0.0005;
    }
    return popDist;
  }

  function adjustForImmigration(popDist) {
    if (immigration > 0.1) {
      popDist[0] += 0.025;
      popDist[1] += 0.01;
      popDist[2] += 0.02;
      popDist[3] -= 0.055;
    } else if (immigration > 0.075) {
      popDist[0] += 0.02;
      popDist[1] += 0.0075;
      popDist[2] += 0.01;
      popDist[3] -= 0.0375;
    } else if (immigration > 0.05) {
      popDist[0] += 0.015;
      popDist[1] += 0.005;
      popDist[2] += 0.005;
      popDist[3] -= 0.025;
    } else if (immigration >= 0.02) {
      popDist[0] += 0.01;
      popDist[1] += 0.0025;
      popDist[2] += 0.0025;
      popDist[3] -= 0.015;
    }
    return popDist;
  }

  function adjustForWage(popDist) {
    const [prevWage, curWage] = wage;
    const delta = (curWage - prevWage) / curWage;

    if (delta < -0.1) {
      popDist[0] -= 0.05;
      popDist[1] -= 0.02;
      popDist[2] -= 0.06;
      popDist[3] += 0.01;
    } else if (delta < -0.5) {
      popDist[0] -= 0.025;
      popDist[1] -= 0.01;
      popDist[2] -= 0.035;
    } else if (delta > 0.25) {
      popDist[0] += 0.01;
      popDist[2] -= 0.01;
    }
    return popDist;
  }

  // Default distribution
  let popDist = [0.22, 0.68, 0.01, 0.09]; // kids, work, unemployed, retired

  popDist = adjustForAge(popDist);
  popDist = adjustForFamily(popDist);
  popDist = adjustForImmigration(popDist);
  popDist = adjustForWage(popDist);

  return popDist;
};

export default CalcPopDist;
