import { currency, error } from "../lib/Lib";

/**
 * Checks form data for errors, display errors under the
 * appropriate form field, and returns the number of errors
 * found.
 */
const CheckFormData = (form, budget) => {
  let {
    newBorrowing,
    repayDebt,
    wages,
    pensionRatio,
    familyRatio,
    farmingRatio,
    immigrationRatio,
    infrastructureRatio,
    propagandaRatio,
  } = form;

  let errCount = 0;

  if (newBorrowing.value < 0) {
    error(
      "It’s not possible to borrow less than $0",
      `error-${newBorrowing.domId}`
    );
    errCount++;
  }

  if (parseInt(repayDebt.value) < parseInt(budget.calcNextDebtRepayment[1])) {
    error(
      `The minimum quarterly debt repayment is ${currency(
        budget.calcNextDebtRepayment[1]
      )}`,
      `error-${repayDebt.domId}`
    );
    errCount++;
  }

  if (wages.value < 0) {
    error(
      "The people need money to survive. Please specify a wage above $0",
      `error-${wages.domId}`
    );
    errCount++;
  }

  if (pensionRatio.value < 0 || pensionRatio.value > 1) {
    error(
      "The pension ratio must be a decimal number greater than 0 but less than 1",
      `error-${pensionRatio.domId}`
    );
    errCount++;
  }

  if (familyRatio.value < 0 || familyRatio.value > 1) {
    error(
      "The family ratio must be a decimal number greater than 0 but less than 1",
      `error-${familyRatio.domId}`
    );
    errCount++;
  }

  if (farmingRatio.value < 0 || farmingRatio.value > 1) {
    error(
      "The farming ratio must be a decimal number greater than 0 but less than 1",
      `error-${farmingRatio.domId}`
    );
    errCount++;
  }

  if (immigrationRatio.value < 0 || immigrationRatio.value > 1) {
    error(
      "The immigration ratio must be a decimal number greater than 0 but less than 1",
      `error-${immigrationRatio.domId}`
    );
    errCount++;
  }

  if (infrastructureRatio.value < 0 || infrastructureRatio.value > 1) {
    error(
      "The infrastructure ratio must be a decimal number greater than 0 but less than 1",
      `error-${infrastructureRatio.domId}`
    );
    errCount++;
  }

  if (propagandaRatio.value < 0 || propagandaRatio.value > 1) {
    error(
      "The propaganda ratio must be a decimal number greater than 0 but less than 1",
      `error-${propagandaRatio.domId}`
    );
    errCount++;
  }

  return errCount++;
};

export default CheckFormData;
