const CalcCostLiving = (data) => {
  const {
    calcCrime,
    calcInflation,
    calcNextDebtRepayment,
    calcTaxRevenue,
    family,
    farming,
    food,
  } = data;

  function calcCrimeImpact() {
    if (calcCrime[9] > 200) {
      return 0.125;
    } else if (calcCrime[9] > 150) {
      return 0.1;
    } else if (calcCrime[9] > 125) {
      return 0.075;
    } else if (calcCrime[9] > 100) {
      return 0.05;
    } else if (calcCrime[9] > 75) {
      return 0.025;
    } else if (calcCrime[9] >= 50) {
      return 0.0125;
    } else if (calcCrime[9] >= 25) {
      return -0.005;
    }
    return -0.05;
  }

  function calcInflationImpact() {
    if (calcInflation[9] > 0.2) {
      return 0.1;
    } else if (calcInflation[9] > 0.05) {
      return 0.075;
    } else if (calcInflation[9] > 0.0075) {
      return 0.05;
    } else if (calcInflation[9] > 0.005) {
      return 0.025;
    } else if (calcInflation[9] > 0.0025) {
      return 0.0125;
    } else if (calcInflation[9] >= 0.001) {
      return -0.001;
    } else if (calcInflation[9] >= 0) {
      return -0.0025;
    } else if (calcInflation[9] < 0.02) {
      return -0.05;
    } else if (calcInflation[9] < 0.01) {
      return -0.1;
    }
    return -0.005;
  }

  function calcDebtToTax() {
    const debtTaxRatio = Number(calcNextDebtRepayment[1] / calcTaxRevenue[1]);

    if (debtTaxRatio >= 0.1) {
      return 0.15;
    } else if (debtTaxRatio >= 0.01) {
      return 0.125;
    } else if (debtTaxRatio >= 0.001) {
      return 0.0895;
    } else if (debtTaxRatio >= 0.0001) {
      return 0.035;
    }
    return 0.01;
  }

  function calcFamily() {
    if (family <= 0.01) {
      return 0.15;
    } else if (family <= 0.02) {
      return 0.125;
    } else if (family <= 0.03) {
      return 0.091;
    } else if (family <= 0.04) {
      return 0.0451;
    }
    return 0.0125;
  }

  function calcFarming() {
    if (farming <= 0.01) {
      return 0.2;
    } else if (farming <= 0.025) {
      return 0.15;
    } else if (farming <= 0.05) {
      return 0.1;
    } else if (farming <= 0.075) {
      return 0.05;
    }
    return 0.025;
  }

  function calcFood() {
    if (food[1] > 1.05) {
      return 0;
    } else if (food >= 1.0) {
      return 0.025;
    } else if (food <= 0.03) {
      return 0.05;
    } else if (food <= 0.04) {
      return 0.025;
    }
    return 0.01;
  }

  return (
    calcCrimeImpact() +
    calcInflationImpact() +
    calcDebtToTax() +
    calcFamily() +
    calcFarming() +
    calcFood()
  );
};

export default CalcCostLiving;
