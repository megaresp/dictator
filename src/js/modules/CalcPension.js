const CalcPension = (data) => {
  const { calcPopDist, pension, pop, wage } = data;

  return wage[1] * pension[1] * calcPopDist[3] * pop[1];
};

export default CalcPension;
