const AgePopulation = (data) => {
  const { age, calcCostLiving, family, immigration } = data;

  function adjustForAge() {
    if (age[1] > 50) {
      return 0.1;
    } else if (age[1] > 40) {
      return 0.05;
    } else if (age[1] > 35) {
      return 0.025;
    } else if (age[1] > 30) {
      return 0.01;
    }
    return 0.005;
  }

  function adjustForFamily() {
    if (family > 0.075) {
      return -0.1;
    } else if (family > 0.05) {
      return -0.05;
    } else if (family > 0.02) {
      return -0.02;
    } else if (family > 0.01) {
      return -0.01;
    } else if (family >= 0.05) {
      return -0.005;
    }
    return 0.01;
  }

  function adjustForImmigration() {
    if (immigration > 0.075) {
      return -0.15;
    } else if (immigration >= 0.05) {
      return -0.1;
    } else if (immigration >= 0.025) {
      return -0.05;
    } else if (immigration > 0.01) {
      return -0.005;
    }
    return 0.05;
  }

  function adjustForCostLiving() {
    if (calcCostLiving[1] > 0.5) {
      return -0.1;
    } else if (calcCostLiving[1] >= 0.4) {
      return -0.075;
    } else if (calcCostLiving[1] >= 0.3) {
      return 0.05;
    }
    return 0.1;
  }

  return (
    age[1] +
    adjustForAge() +
    adjustForCostLiving() +
    adjustForFamily() +
    adjustForImmigration()
  );
};

export default AgePopulation;
