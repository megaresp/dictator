const GrowPopulation = (data) => {
  const { age, calcCostLiving, family, immigration, pop } = data;

  function adjustForAge() {
    if (age[1] >= 70) {
      return pop[1] * 0.94;
    } else if (age[1] >= 60) {
      return pop[1] * 0.985;
    } else if (age[1] >= 50) {
      return pop[1] * 1.005;
    } else if (age[1] >= 40) {
      return pop[1] * 1.0075;
    } else if (age[1] > 31) {
      return pop[1] * 1.011;
    } else if (age[1] > 21) {
      return pop[1] * 1.02;
    }
    return pop[1] * 1.004;
  }

  function adjustForCostLiving() {
    if (calcCostLiving[1] > 0.5) {
      return pop[1] * 0.96;
    } else if (calcCostLiving[1] > 0.4) {
      return pop[1] * 0.98;
    } else if (calcCostLiving[1] > 0.35) {
      return pop[1] * 1.005;
    } else if (calcCostLiving[1] > 0.3) {
      return pop[1] * 1.01;
    } else if (calcCostLiving[1] > 0.25) {
      return pop[1] * 1.015;
    }
    return pop[1] * 1.02;
  }

  function adjustForFamily() {
    if (family > 0.075) {
      return pop[1] * 1.02;
    } else if (family > 0.04) {
      return pop[1] * 1.01;
    } else if (family > 0.035) {
      return pop[1] * 1.005;
    } else if (family > 0.03) {
      return pop[1] * 1.0025;
    } else if (family > 0.025) {
      return pop[1] * 1.001;
    } else if (family > 0.01) {
      return pop[1] * 1.0005;
    }
    return pop[1];
  }

  function adjustForImmigration() {
    if (immigration > 0.075) {
      return pop[1] * 1.05;
    } else if (immigration > 0.05) {
      return pop[1] * 1.03;
    } else if (immigration > 0.025) {
      return pop[1] * 1.01;
    } else if (immigration > 0.01) {
      return pop[1] * 1.005;
    } else if (immigration > 0.005) {
      return pop[1] * 1.001;
    } else if (immigration > 0.0025) {
      return pop[1] * 1.0005;
    }
    return pop[1];
  }

  return (
    (adjustForAge() +
      adjustForCostLiving() +
      adjustForFamily() +
      adjustForImmigration()) /
    4
  );
};

export default GrowPopulation;
