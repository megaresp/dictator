import CalcCostLiving from "./CalcCostLiving";
import CalcHealth from "./CalcHealth";
import CalcHappiness from "./CalcHappiness";
// import CalcInflation from "./CalcInflation";
import CalcNextDebtRepayment from "./CalcNextDebtRepayment";
import CalcPopDist from "./CalcPopDist";
import CalcPension from "./CalcPension";
import CalcTaxRevenue from "./CalcTaxRevenue";
import CalcCrime from "./CalcCrime";

const BudgetObject = {
  age: [28.5, 29], // Average age of the population
  debt: 100000000, // Amount the government owes
  debtInterest: 0.01, // The interest rate the government pays per turn
  debtPrinciple: 0.0125, // The percentage of the debt that must be repaid per turn
  defence: 0.025, // The % of revenue spent on defence
  family: 0.005, // % of government revenue invested in the family - more means younger population
  farming: 0.025, // % of government revenue invested in farming - more means more food
  food: [1.03, 1.025], // Food available to feed the population. 1.01 means 101% more food produced than consumed
  immigration: 0.005, // % of government revenue spent on attracting immigrants - more immigrants
  infrastructure: 0.0975, // % of government revenue spent on infrastructure - roads, power stations, govt employees
  law: 0.05, // % of revenue spent on law enforcement and the courts
  pension: [0.4, 0.4], // % of average wage to pay as pension
  pop: [1002057, 1017432], // Number of people living in country
  propaganda: 0.0107, // % of government revenue spent on propaganda - more means happier people, too much makes them angry
  tax: [0.3, 0.29], // The previous and current tax rate
  treasury: [17526192, 19278086], // Previous and current cash surplus
  wage: [1750, 1776, 0], // Previous, current average wage, % of workers employed by the government
};

BudgetObject.wage[2] = BudgetObject.infrastructure * 1.75; // Increases to infrastructure increase govt. employees %

BudgetObject.calcHealth = [0.58, 0.61];
BudgetObject.calcCostLiving = [0.41, 0.51];
BudgetObject.calcHappiness = [0.66, 0.66];
(BudgetObject.calcCrime = [45, 71, 128, 135, 62, 19, 37, 78, 44, 42]), // Crimes per 1,000 people
  (BudgetObject.calcInflation = [
    0.0924, 0.0891, 0.082, 0.07902, 0.0705, 0.0711, 0.07506, 0.07945, 0.083,
    0.0796,
  ]);
BudgetObject.calcNextDebtRepayment = [225000, 225000];
BudgetObject.calcPopDist = [0.225, 0.68, 0.01, 0.085];
BudgetObject.calcPension = [56610000.0, 56610000.0];
BudgetObject.calcTaxRevenue = [394356643.2, 394356643.2];

// The following properties are calculated from above fixed properties
// BudgetObject.calcHealth[1] = CalcHealth(BudgetObject); // A real number from 0-1. Higher is a healthier population
BudgetObject.calcHappiness[1] = CalcHappiness(BudgetObject); // A real number from 0-1. Higher is a happier population
BudgetObject.calcNextDebtRepayment[1] = CalcNextDebtRepayment(BudgetObject); // The next debt repayment
BudgetObject.calcPopDist = CalcPopDist(BudgetObject); // returns array containing percentage of pop that are [kids, work, unemployed, retired]
BudgetObject.calcPension[1] = CalcPension(BudgetObject); // returns the amount of money the nation must spend on pensions
BudgetObject.calcTaxRevenue[1] = CalcTaxRevenue(BudgetObject); // The amount of money the government earns from taxation

// cost of living calc must go second to last
BudgetObject.calcCostLiving[1] = CalcCostLiving(BudgetObject); // Percentage of income spent of living costs

BudgetObject.calcCrime = CalcCrime(BudgetObject); // The crime rate

// Inflation calc must go last
// BudgetObject.calcInflation = CalcInflation(BudgetObject); // The rate of inflation

export default BudgetObject;
