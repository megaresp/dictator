import { shiftArrayLeft } from "../lib/Lib";

const CalcCrime = (data) => {
  const { calcCostLiving, calcCrime, family, immigration } = data;

  function adjustForCostLiving() {
    if (calcCostLiving[1] > 0.5) {
      return calcCrime[9] * 1.0504;
    } else if (calcCostLiving[1] > 0.4) {
      return calcCrime[9] * 1.027;
    } else if (calcCostLiving[1] > 0.35) {
      return calcCrime[9] * 1.0053;
    } else if (calcCostLiving[1] > 0.3) {
      return calcCrime[9] * 0.999;
    } else if (calcCostLiving[1] > 0.25) {
      return calcCrime[9] * 0.987;
    }
    return calcCrime[9] * 0.961;
  }

  function adjustForFamily() {
    if (family > 0.075) {
      return calcCrime[9] * 0.95;
    } else if (family > 0.04) {
      return calcCrime[9] * 0.97;
    } else if (family > 0.035) {
      return calcCrime[9] * 0.981;
    } else if (family > 0.03) {
      return calcCrime[9] * 0.992;
    } else if (family > 0.025) {
      return calcCrime[9] * 0.997;
    } else if (family > 0.01) {
      return calcCrime[9] * 0.999;
    }
    return calcCrime[9] * 1.0063;
  }

  function adjustForImmigration() {
    if (immigration > 0.075) {
      return calcCrime[9] * 1.0593;
    } else if (immigration > 0.05) {
      return calcCrime[9] * 1.0368;
    } else if (immigration > 0.025) {
      return calcCrime[9] * 1.0104;
    } else if (immigration > 0.01) {
      return calcCrime[9] * 1.0016;
    } else if (immigration > 0.005) {
      return calcCrime[9] * 1.0005;
    } else if (immigration > 0.0025) {
      return calcCrime[9] * 1.0002;
    }
    return calcCrime[9] * 0.992;
  }

  const newCrimeRate =
    (adjustForCostLiving() + adjustForFamily() + adjustForImmigration()) / 3;

  return shiftArrayLeft(data.calcCrime, newCrimeRate);
};

export default CalcCrime;
