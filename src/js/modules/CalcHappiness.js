const CalcHappiness = (data) => {
  const {
    calcCostLiving,
    family,
    food,
    immigration,
    infrastructure,
    propaganda,
    tax,
    wage,
  } = data;

  function calcCostLivingScore() {
    if (calcCostLiving[1] <= 0.25) {
      return 0.15;
    } else if (calcCostLiving[1] <= 0.33) {
      return 0.1;
    } else if (calcCostLiving[1] <= 0.4) {
      return 0.05;
    }
    return 0.025;
  }

  function calcFamilyScore() {
    if (family > 0.075) {
      return 0.82;
    } else if (family > 0.05) {
      return 0.15;
    } else if (family > 0.025) {
      return 0.1;
    } else if (family >= 0.01) {
      return 0.05;
    }
    return 0;
  }

  function calcFoodScore() {
    if (food[1] > 1.1) {
      return 0.25;
    } else if (food[1] > 1.05) {
      return 0.2;
    } else if (food[1] > 1) {
      return 0.15;
    } else if (food[1] >= 0.95) {
      return 0.1;
    }
    return 0;
  }

  function calcImmigrationScore() {
    if (immigration < 0.02) {
      return 0.1;
    } else if (immigration <= 0.01) {
      return 0.15;
    } else if (immigration < 0.005) {
      return 0.05;
    }
    return 0;
  }

  function calcInfrastructureScore() {
    if (infrastructure > 0.08) {
      return 0.15;
    } else if (infrastructure > 0.04) {
      return 0.1;
    } else if (infrastructure >= 0.02) {
      return 0.05;
    }
    return 0;
  }

  function calcPropagandaScore() {
    if (propaganda > 0.1) {
      return -0.05;
    } else if (infrastructure > 0.05) {
      return 0.1;
    } else if (infrastructure > 0.025) {
      return 0.05;
    } else if (infrastructure >= 0.01) {
      return 0.025;
    }
    return 0;
  }

  function calcTaxScore() {
    const taxDelta = (tax[1] - tax[0]) / tax[1];

    if (taxDelta <= -0.1) {
      return 0.1;
    } else if (taxDelta <= -0.05) {
      return 0.05;
    } else if (taxDelta > 0.25) {
      return -0.1;
    } else if (taxDelta > 0.1) {
      return -0.05;
    }

    return 0;
  }

  function calcWageScore() {
    const [prevWage, curWage] = wage;
    const delta = (curWage - prevWage) / curWage;

    if (delta > 0.15) {
      return 0.2;
    } else if (delta > 0.11) {
      return 0.175;
    } else if (delta > 0.075) {
      return 0.15;
    } else if (delta > 0.05) {
      return 0.1;
    } else if (delta > 0.025) {
      return 0.05;
    } else if (delta > 0.01) {
      return 0;
    } else if (delta <= 0.01) {
      return -0.01;
    }
    return -0.025;
  }

  return (
    0.05 +
    calcCostLivingScore() +
    calcFamilyScore() +
    calcFoodScore() +
    calcImmigrationScore() +
    calcInfrastructureScore() +
    calcPropagandaScore() +
    calcTaxScore() +
    calcWageScore()
  );
};

export default CalcHappiness;
