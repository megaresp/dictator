import {
  calcExpenditure,
  currency,
  error,
  formIsEmpty,
  hide,
  notify,
  plural,
  save,
} from "../lib/Lib";
import CalcCostLiving from "./CalcCostLiving";
import CalcHealth from "./CalcHealth";
import CalcHappiness from "./CalcHappiness";
import CalcInflation from "./CalcInflation";
import CalcPension from "./CalcPension";
import CalcNextDebtRepayment from "./CalcNextDebtRepayment";
import CalcPopDist from "./CalcPopDist";
import CalcTaxRevenue from "./CalcTaxRevenue";
import CheckFormData from "./CheckFormData";
import AgePopulation from "./AgePopulation";
import GrowPopulation from "./GrowPopulation";
import CalcCrime from "./CalcCrime";

const UpdateBudget = (
  budget,
  form,
  setForm,
  year,
  setYear,
  quarter,
  setQuarter
) => {
  let errCount = 0;
  let noticeCount = 0;

  hide(".formFieldError, #formError");

  // Scan for errors only if the form has data
  if (!formIsEmpty(form)) {
    errCount = CheckFormData(form, budget);
  }

  if (errCount > 0) {
    error(
      `${errCount === 1 ? "One" : errCount} error${plural(
        errCount
      )} found. Please fix and resubmit your budget`,
      "formError"
    );
  } else {
    // No errors found so process the new budget

    if (quarter === 4) {
      setYear(year + 1);
      setQuarter(1);
    } else {
      setQuarter(quarter + 1);
    }

    let {
      newBorrowing,
      repayDebt,
      taxRate,
      wages,
      pensionRatio,
      defenceRatio,
      familyRatio,
      farmingRatio,
      immigrationRatio,
      infrastructureRatio,
      lawRatio,
      propagandaRatio,
    } = form;

    budget.treasury[0] = budget.treasury[1];
    // Add tax take to treasury
    budget.treasury[1] += budget.calcTaxRevenue[1];

    if (newBorrowing.value > 0) {
      budget.debt += newBorrowing.value; // Add new borrowing to debt
      budget.treasury[1] += newBorrowing.value; // Add new debt to treasury
    }

    // Deduct this quarter's expenditure
    budget.treasury[1] -= calcExpenditure(budget);
    /**
     * Player doesn't have enough money in the treasury to meet expenses.
     * Two options here:
     *    1. Player's debt to tax ratio < 7: autoborrow money to meet shortfall
     *    2. Player's debt to tax ratio >= 7: end the game
     */
    if (budget.treasury[1] < 0) {
      if (budget.debt / budget.calcTaxRevenue[1] < 7) {
        let amountBorrowed = Math.abs(budget.treasury[1]) + repayDebt.value;
        budget.debt += amountBorrowed;
        budget.treasury[1] = repayDebt.value; // Add debt repayment to treasury
        notify(
          "Important Notice!",
          `The treasury was forced to borrow ${currency(
            amountBorrowed
          )} to meet a shortfall in funds. Be careful, this is exactly the sort of financial mismanagement that may lead to a dictator being repurposed as fertliser.`
        );
        noticeCount++;
      } else {
        const reason =
          "The nation has no money! During your reign you borrowed seven times more than our small nation earns. Nobody is willing to loan us more money and we are now bankrupt.";
        save("GameOver", budget, year, quarter, reason);
        setTimeout(() => {
          location.href = "/gameover";
        }, 100);
      }
    }

    if (repayDebt.value > 0) {
      // Does player have enough money in treasuring to pay down debt?
      if (repayDebt.value > budget.treasury[1]) {
        const reason =
          "Thanks to an incompetent approach to economic management the nation finds itself with too little money to meet its minimum debt repayment.";
        save("GameOver", budget, year, quarter, reason);
        setTimeout(() => {
          location.href = "/gameover";
        }, 100);
      }
      budget.debt -= repayDebt.value;
      budget.treasury[1] -= repayDebt.value;
    }

    budget.tax[0] = budget.tax[1];
    budget.tax[1] = taxRate.value;

    budget.wage[0] = budget.wage[1];
    budget.wage[1] = wages.value; // Set new current wage

    /** Update ratios **/

    budget.pension[0] = budget.pension[1];
    budget.pension[1] = pensionRatio.value;
    if (pensionRatio.value > 0) {
      budget.calcPension[1] = CalcPension(budget);
    }

    budget.defence = defenceRatio.value;
    budget.family = familyRatio.value;
    budget.farming = farmingRatio.value;
    budget.immigration = immigrationRatio.value;
    budget.infrastructure = infrastructureRatio.value;
    budget.law = lawRatio.value;
    budget.propaganda = propagandaRatio.value;

    // This sets the % of workers employed by the government
    budget.wage[2] = budget.infrastructure * 1.75;

    budget.calcHealth[0] = budget.calcHealth[1];
    budget.calcHealth[1] = CalcHealth(budget);

    budget.calcHappiness[0] = budget.calcHappiness[1];
    budget.calcHappiness[1] = CalcHappiness(budget);

    budget.calcNextDebtRepayment[0] = budget.calcNextDebtRepayment[1];
    budget.calcNextDebtRepayment[1] = CalcNextDebtRepayment(budget);

    budget.calcPopDist = CalcPopDist(budget); // returns array containing % pop for [kids, work, unemployed, retired]

    budget.calcTaxRevenue[1] = CalcTaxRevenue(budget); // The amount of money the government earns from taxation
    budget.calcCostLiving[1] = CalcCostLiving(budget); // The cost of living for citizens

    budget.age[0] = budget.age[1];
    budget.age[1] = AgePopulation(budget);

    budget.pop[0] = budget.pop[1];
    budget.pop[1] = GrowPopulation(budget);

    budget.calcCrime = CalcCrime(budget);
    budget.calcInfation = CalcInflation(budget);

    if (noticeCount === 0) {
      hide("#update-notice");
    } else {
      noticeCount = 0;
    }

    // Animate in subheadings
    const subheadings = document.querySelectorAll(".report section h2 div");
    subheadings.forEach((ele) => {
      ele.style.opacity = 0;
      ele.classList.remove("wave");
      setTimeout(() => {
        ele.classList.add("wave");
        ele.style.opacity = 1;
      }, 333);
    });

    window.scrollTo(0, 0);
  }
};

export default UpdateBudget;
