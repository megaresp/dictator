const CalcHealth = (data) => {
  const { age, calcCostLiving, family, food, infrastructure } = data;

  function calcAgeHealth() {
    if (age[1] > 60) {
      return 0.01;
    } else if (age[1] > 50) {
      return 0.022;
    } else if (age[1] > 40) {
      return 0.065;
    } else if (age[1] > 30) {
      return 0.11;
    }
    return 0.2;
  }

  function calcCostLivingHealth() {
    if (calcCostLiving[1] < 0.25) {
      return 0.2;
    } else if (calcCostLiving[1] < 0.4) {
      return 0.1;
    } else if (calcCostLiving[1] < 0.5) {
      return 0.04;
    } else if (calcCostLiving[1] < 0.6) {
      return 0.02;
    } else if (calcCostLiving[1] < 0.7) {
      return 0.01;
    }
    return 0;
  }

  function calcFamilyHealth() {
    if (family > 0.075) {
      return 0.2;
    } else if (family > 0.05) {
      return 0.15;
    } else if (family > 0.025) {
      return 0.1;
    } else if (family >= 0.01) {
      return 0.05;
    }
    return 0.01;
  }

  function calcFoodHealth() {
    if (food[1] > 1.1) {
      return 0.2;
    } else if (food[1] > 1.05) {
      return 0.15;
    } else if (food[1] > 1) {
      return 0.1;
    } else if (food[1] >= 0.95) {
      return 0.05;
    } else if (food[1] >= 0.8) {
      return 0.02;
    }
    return 0.1;
  }

  function calcInfrastructure() {
    if (infrastructure >= 0.25) {
      return -0.05;
    } else if (infrastructure > 0.125) {
      return 0.025;
    } else if (infrastructure >= 0.1) {
      return 0.1;
    } else if (infrastructure > 0.075) {
      return 0.0667;
    } else if (infrastructure > 0.05) {
      return 0.035;
    } else if (infrastructure > 0.025) {
      return 0.01;
    }
    return -0.1;
  }

  return (
    0.2 +
    calcAgeHealth() +
    calcCostLivingHealth() +
    calcFamilyHealth() +
    calcFoodHealth() +
    calcInfrastructure()
  );
};

export default CalcHealth;
