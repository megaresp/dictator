const CalcTaxRevenue = (data) => {
  const { calcPopDist, pop, tax, wage } = data;

  return calcPopDist[1] * pop[1] * tax[1] * wage[1];
};

export default CalcTaxRevenue;
