const CalcNextDebtRepayment = (data) => {
  const { debt, debtInterest, debtPrinciple } = data;

  return debt * debtInterest + debt * debtPrinciple;
};

export default CalcNextDebtRepayment;
