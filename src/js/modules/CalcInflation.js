import { shiftArrayLeft } from "../lib/Lib";

const CalcInflation = (data) => {
  const { calcInflation, calcCostLiving, calcCrime, propaganda, wage } = data;

  const deltaInflation =
    calcInflation.reduce((partialSum, val) => partialSum + val, 0) /
    calcInflation.length;
  const deltaCostLiving =
    (calcCostLiving[1] - calcCostLiving[0]) / calcCostLiving[0];
  const deltaCrime = (calcCrime[9] - calcCrime[8]) / calcCrime[8];
  const deltaWage = (wage[1] - wage[0]) / wage[0];

  function propagandaScore() {
    if (propaganda > 0.1) {
      return 0.05;
    } else if (propaganda > 0.05) {
      return -0.05;
    } else if (propaganda > 0.025) {
      return 0.001;
    } else if (propaganda > 0.0125) {
      return 0.0025;
    }
    return 0.005;
  }

  const newInflation =
    deltaCostLiving +
    deltaCrime +
    deltaInflation +
    propagandaScore() +
    deltaWage;

  return shiftArrayLeft(data.calcInflation, newInflation);
};

export default CalcInflation;
